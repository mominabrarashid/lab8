#include "matrix.h"
#include "vector.h"
#include <iostream>
#include <pthread.h>

void main(){

	pthread_t t[32];
	matClass A(5, 5);
	matClass B(5, 5);
	for (int x = 0; x < 32; x++){
		pthread_create(&t[x], NULL, vecMultiply, x);
		pthread_join(t[x], NULL);
	}
	matMultiply(A,B);
	cout << "Code ran without any errors."<< endl;

}