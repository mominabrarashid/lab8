OBJ  = vector.o matrix.o main.o
prog : $(OBJ)
	g++ -std=c++0x -o prog $(OBJ)
vector.o : vector.cc vector.h  
	g++ -std=c++0x -c vector.cc
matrix.o : matrix.cc matrix.h
	g++ -std=c++0x -c matrix.cc
main.o : main.cc vector.h matrix.h 
	g++ -pthread -std=c++0x -c main.cc 
clean :
	rm $(OBJ)